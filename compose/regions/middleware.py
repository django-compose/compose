# -*- coding: utf-8 -*-
import os

from compose.views import registry
from django.utils.deprecation import MiddlewareMixin

class RegionInfoMiddleware(MiddlewareMixin):

    def process_view(self, request, view_func, view_args, view_kwargs):
        if view_func.__name__ in registry:
            request.region_info = RegionInfo(request, view_func, view_args, view_kwargs)

class RegionInfo:
    def __init__(self, request, view_func, view_args, view_kwargs):
        self.module_name = view_func.__module__
        self.view_name = view_func.__name__.lower()
        self.view_cls_tpl_pth = view_func.cls.tpl_path

    def get_templates_format(self):
        return "html"

    def get_view_path(self):
        return os.path.join(os.path.join(*self.module_name.split('.')), self.view_name)

    def get_template_names(self, region):
        region_template_file = "%s.%s" % (region, self.get_templates_format())
        
        return [os.path.join(self.get_view_path(), region_template_file),
                os.path.join(self.view_cls_tpl_pth, region_template_file),
                region_template_file]
        
