import os

from django import template
from django.template.loader import get_template

from django.conf import settings
from django.template.loader import render_to_string

register = template.Library()

@register.simple_tag(takes_context=True)
def region(context, name):
    print(context)
    content = None
    tpl_search_pth = context['request'].region_info.get_template_names(name)
    if settings.DEBUG:
        # just for debug: print out the template name we're looking for
        print(tpl_search_pth)
    # tries to get template from list of template paths
    tpl = context.template.engine.select_template(tpl_search_pth)
    content = tpl.render(context)
    if content is None:
        raise template.TemplateDoesNotExist('You should provide `%s` region template. Template search path is: %s' % (name, tpl_search_pth))
    return content




