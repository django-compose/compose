import importlib

def merge_settings(settings, *imports):
    for settings_name in imports:
        module = importlib.import_module(settings_name)
        settings_attrs = tuple((attr for attr in dir(module) if attr.isupper()))
        settings.update(dict((attr_name, getattr(module, attr_name)) for attr_name in settings_attrs))


def load_settings_attrs(settings, settings_attrs, settings_name):
    module = importlib.import_module(settings_name)
    module_settings_attrs = tuple((attr for attr in dir(module) if attr.isupper()))
    settings.update(dict((attr_name, getattr(module, attr_name)) for attr_name in settings_attrs if attr_name in module_settings_attrs))


