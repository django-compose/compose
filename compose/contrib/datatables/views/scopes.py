import os
from compose.views.scopes import Section
from .main import DataTableMixin

class ComposeDataTableSection(DataTableMixin, Section):
    render_on_init = True

    def get_content_tpl(self):
        return 'composedatatablesection/content.html'

    def get_js_tpl(self):
        return 'composedatatablesection/data_table_js.html'

    def get_tpl_path(self, tpl_name):
        module_name = self.__module__
        view_name = self.__class__.__name__.lower()
        return os.path.join(os.path.join(*module_name.split('.')), view_name, self.name, '%s.html' % tpl_name)


