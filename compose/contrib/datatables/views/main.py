import json
import os
from compose.views import ComposeListView
from django.core.serializers import serialize
from django.db.models import Q
from django.http import JsonResponse
from django.template.loader import get_template
from operator import __or__ as OR
from functools import reduce


class DTField:

    def __init__(self, **kwargs):
        defaults = {
            'lookup': None,
            'handler': None,
            'tpl_name': None,
            'searchable': False,
            'orderable': False,
            'responsive_priority': 10000,
        }
        defaults.update(kwargs)
        for key, value in defaults.items():
            setattr(self, key, value)


class DataTableMixin:
    extra_search_lookups = ()
    responsive = False

    def get_fields(self):
        if not self.fields:
            raise ImproperlyConfigured("No fields defined.")
        return self.fields

    def get_dt_fields(self):
        if hasattr(self, 'dt_fields'):
            return self.dt_fields
        self.dt_fields = []
        for field in self.get_fields():
            self.dt_fields.append(DTField(**field))
        return self.dt_fields

    def get_json_data(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        queryset_count = queryset.count()
        queryset = self.order_queryset(queryset, request)
        queryset = self.filter_queryset(queryset, request)
        filtered_queryset_count = queryset.count()
        queryset = self.paginate(queryset, request)
        queryset = self.process_data(queryset)
        return {
            'data': queryset,
            'recordsTotal': queryset_count,
            'recordsFiltered': filtered_queryset_count,
            'draw': request.GET.get('draw'),
        }

    def filter_queryset(self, queryset, request):
        if request.GET.get('search[value]'):
            filter_queries = []
            for field in filter(lambda x: x.get('searchable'), self.get_fields()):
                filter_queries.append(Q(**{'%s__icontains' % field['lookup']: request.GET.get('search[value]')}))
            for lookup in self.extra_search_lookups:
                filter_queries.append(Q(**{'%s__icontains' % lookup: request.GET.get('search[value]')}))
            if filter_queries:
                queryset = queryset.filter(reduce(OR, filter_queries))
        return queryset

    def order_queryset(self, queryset, request):
        if request.GET.get('order[0][column]'):
            order_field = self.get_dt_fields()[int(request.GET.get('order[0][column]'))]
            order_lookup = order_field.lookup
            if request.GET.get('order[0][dir]') == 'desc':
                order_lookup = '-' + order_lookup
            queryset = queryset.order_by(order_lookup)
        return queryset

    def paginate(self, queryset, request):
        paginate_by = request.GET.get('length') or self.paginate_by  # objects per page
        paginate_by = int(paginate_by)
        first_item = int(request.GET.get('start'))
        return queryset[first_item:first_item + paginate_by]

    def process_data(self, queryset):
        lookups = [field['lookup'] for field in self.get_fields()]
        values_list = [list(item) for item in queryset.values_list(*lookups)]
        object_list = [item for item in queryset]
        # wrapping with handlers
        for col_number, handler_name in [(num, e.get('handler')) for num, e in enumerate(self.get_fields()) if e.get('handler')]:
            handler = getattr(self, handler_name)
            for item_number, item in enumerate(values_list):
                item[col_number] = handler(object_list[item_number], item[col_number])
        # rendering templates
        for col_number, tpl_name in [(num, e.get('tpl_name')) for num, e in enumerate(self.get_fields()) if e.get('tpl_name')]:
            tpl_path = self.get_tpl_path(tpl_name)
            tpl = get_template(tpl_path)
            for item_number, item in enumerate(values_list):
                ctx = {'object': object_list[item_number]}
                item[col_number] = tpl.render(ctx)
        return list(values_list)

    def get_extra_context(self, *args, **kwargs):
        context = {}
        context['fields'] = self.get_fields()
        context['dt_fields'] = self.get_dt_fields()
        context['responsive'] = self.responsive
        context['paginate_by'] = self.paginate_by
        return context


class ComposeDataTableView(DataTableMixin, ComposeListView):
    tpl_path = 'composedatatableview'
    responsive = True

    def get_context_data(self, *args, **kwargs):
        self.object_list = []
        context = super().get_context_data(*args, **kwargs)
        context.update(self.get_extra_context())
        return context

    def get_tpl_path(self, tpl_name):
        module_name = self.__module__
        view_name = self.__class__.__name__.lower()
        return os.path.join(os.path.join(*module_name.split('.')), view_name, '%s.html' % tpl_name)
