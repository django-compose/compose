from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from compose.views import registry

def patterns():
    """Generates the list of URL paths for the Compose views having the
    `urls` attribute set.
    """
    
    paths = []
    for view_name in filter(lambda reg: registry.get(reg).urls is not None, registry):
        view_cls = registry.get(view_name)
        for name in view_cls.urls:
            pth = [view_cls.url_prefix] if view_cls.url_prefix else []
            pth.append(view_cls.urls.get(name))
            pth = '/'.join(pth)
            paths.append(path(pth, view_cls.as_view(), name=name))
    return paths


