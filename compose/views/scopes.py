import inspect
import os
from django.template import loader

class Link:

    def __init__(self, scope, title, url=None, enabled=True, 
                 templated=False, items=None):
        self.scope = scope
        self.title = title
        self.url = url
        self.enabled = enabled
        self.templated = templated
        
        self.items = None
        if items:
            self.items = []
            for item_kwargs in items:
                self.items.append(Link(scope, **item_kwargs))

    def get_title(self):
        return self.title

class SEO:
    '''
    SEO class returns dict with keys: title, description and keywords. 
    '''

    def __init__(self, scope, title='', description='', keywords='', enabled=True,
                 templated=False, items=None):
        self.scope = scope
        self.title = title
        self.description = description
        self.keywords = keywords
        self.enabled = enabled
        self.templated = templated

        self.items = None
        if items:
            self.items = []
            for item_kwargs in items:
                if item_kwargs.get('enabled', True):
                    self.items.append(SEO(scope, **item_kwargs))

        def get_title(self):
            return self.title

        def get_description(self):
            return self.description

        def get_keywords(self):
            return self.keywords

class Section:
    render_on_init = False
    refresh_rate = None
    
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
    
    def set_scope(self, scope):
        self.scope = scope

    def get_content_tpl(self):
        return os.path.join(self.scope.get_view_path(), "%s.html" % self.name)

    def get_title_tpl(self):
        return os.path.join(self.get_view_path(), "%s_title.html" % self.name)

    def get_js_tpl(self):
        return 'composesection/section_js.html'

    def get_title(self):
        return self.title

    def get_json_data(self, request, *args, **kwargs):
        tpl = loader.get_template(self.get_content_tpl())
        ctx = self.view.get_context_data(**kwargs)
        return {
            'html': tpl.render(ctx),
        }
    
class Scope:

    def __init__(self, view, sections, links, actions, breadcrumbs, seo):
        self.view = view
        self.sections = self.get_sections(sections) if sections else None
        self.links = self.get_links(links) if links else None
        self.actions = self.get_actions(actions) if actions else None
        self.breadcrumbs = [Link(self, **args) for args in breadcrumbs] if breadcrumbs else None
        self.seo = SEO(self, **seo) if seo else None

    def get_sections(self, sections):
        ret = []
        for section in sections:
            if isinstance(section, Section):
                section_instance = section
            elif section.get('enabled', True):
                section_instance = Section(**section)
            section_instance.set_scope(self)
            section_instance.view = self.view
            section_instance.request = self.view.request
            ret.append(section_instance)
        return ret

    def get_links(self, links):
        ret = []
        for kwargs in links:
            ret.append(Link(self, **kwargs))
        return ret

    def get_actions(self, actions):
        ret = []
        for kwargs in actions:
            ret.append(Link(self, **kwargs))
        return ret
 
    def get_view_path(self):
        return os.path.join(os.path.join(*self.view.__module__.split('.')), self.view.__class__.__name__.lower())
