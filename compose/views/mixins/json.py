from django.http import JsonResponse

class ComposeJsonResponseMixin:

    def get_json_data(self, request, *args, **kwargs):
        raise NotImplementedError('You should provide `get_json_data` in your view')

    def get_json_response(self, request, *args, **kwargs):
        self.pre_json_response()
        return JsonResponse(
            self.get_json_data(request, *args, **kwargs)
        )

    def pre_json_response(self):
        pass
