import os
from compose.views.scopes import Scope
from compose.views.mixins import ComposeJsonResponseMixin
from compose.settings import compose_settings

from django.contrib.auth.mixins import LoginRequiredMixin, AccessMixin
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.http import HttpResponseForbidden, HttpResponse
from django.forms import models as model_forms
from django.views.generic import View, TemplateView, DetailView, ListView, FormView, CreateView, DeleteView, UpdateView


class UnsupportedMediaType(HttpResponse):
    status_code = 415


registry = {}

class ComposeBase(type):

    def __new__(cls, name, bases, attrs):
        _new = super(ComposeBase, cls).__new__(cls, name, bases, attrs)
        try:
            if issubclass(_new, ComposeView) and not _new.__name__.startswith('Compose'):
                globals()['registry'][_new.__name__] = _new
        except NameError:
            return _new
        return _new

    
class ComposeView(View, AccessMixin, metaclass=ComposeBase):
    permission_classes = compose_settings.DEFAULT_PERMISSION_CLASSES
    urls = None
    url_prefix = None
    settings = compose_settings
    raise_exception = False # AccessMixin setting - False redirect to login-url
    allowed_content_types = ['html', 'json']

    @classmethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        # https://stackoverflow.com/questions/20715552/accessing-the-current-view-class-instance-in-django-middleware
        view.cls = cls
        return view

    def dispatch(self, request, *args, **kwargs):
        # if self.get_request_content_type(request) not in self.allowed_content_types:
        #     return UnsupportedMediaType
        no_permision_handler = self.check_permissions(request)
        if no_permision_handler:
            return no_permision_handler
        return super().dispatch(request, *args, **kwargs)

    def get_request_content_type(self, request):
        if request.is_ajax():
            return 'json'
        return request.GET.get('format', 'html')

    def permission_denied(self, request, message=None):
        # set permission denied message, used by AccessMixin
        self.permission_denied_message = message
        # and call AccessMixin method now
        return self.handle_no_permission()

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        return [permission() for permission in self.permission_classes]

    def check_permissions(self, request):
        """
        Check if the request should be permitted.
        Raises an appropriate exception if the request is not permitted.
        """
        for permission in self.get_permissions():
            if not permission.has_permission(request, self):
                return self.permission_denied(
                    request, message=getattr(permission, 'message', None)
                )

    def check_object_permissions(self, request, obj):
        """
        Check if the request should be permitted for a given object.
        Raises an appropriate exception if the request is not permitted.
        """
        for permission in self.get_permissions():
            if not permission.has_object_permission(request, self, obj):
                return self.permission_denied(
                    request, message=getattr(permission, 'message', None)
                )
    
    def get_html_response(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        ct = self.get_request_content_type(request)
        return getattr(self, 'get_%s_response' % ct)(request, *args, **kwargs)


class ComposeTemplateViewMixin:
    """
    Links/actions format example:
             ({'title': 'Link name', 'url': 'URL address', 'enabled': True or False, 'templated': True or False},
              {'title': 'Another link', 'url': reverse('url-name'), 'enabled': is_this_link_enabled, 'templated': is_this_link_title_templated},
              {'title': 'Simple one', 'url': '/home/'},
              # Nested links example:
              {'title': 'Nested links group name', 'items': ({'title': 'Nested link name', 'url': 'Nested URL'},)})
    """
    
    tpl_path = 'composetemplateview'
    title = None
    sections = None
    actions = None
    links = None
    breadcrumbs = None
    seo = None
    
    def get_template_names(self):
        return ['page.html']

    def get_title(self):
        """Return the title of this page."""
        if not self.title:
            raise ImproperlyConfigured("No title. Provide a title either as view attr or `get_title` method.")
        return str(self.title)  # title may be lazy

    def get_actions(self):
        return self.actions

    def get_breadcrumbs(self):
        return self.breadcrumbs

    def get_seo(self):
        return self.seo
    
    def get_links(self):
        """Should return the tuple of dictionaries representing links related to this view scope """
        return self.links
    
    def get_sections(self):
        if not self.sections:
            return ({'name': self.__class__.__name__.lower(), 'title': self.get_title()},)
        return self.sections
    
    def get_scope(self, is_ajax=False):
        if is_ajax:
            return Scope(self, self.get_sections(), None, None, None, None)
        return Scope(self, self.get_sections(), self.get_links(), self.get_actions(), self.get_breadcrumbs(), self.get_seo())
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.get_title()
        context['scope'] = self.get_scope()
        context['view_name'] = self.__class__.__name__.lower()
        return context
    
    def get_json_data(self, request, *args, **kwargs):
        section_name = request.GET.get('section')
        sections = dict((section.name, section) for section in self.get_scope(is_ajax=True).sections)
        if section_name and section_name in sections:
            section = sections.get(section_name)
            return section.get_json_data(request, *args, **kwargs)


class ComposeTemplateView(ComposeView, ComposeTemplateViewMixin, ComposeJsonResponseMixin, TemplateView):
    pass


class ComposeDetailView(ComposeView, ComposeTemplateViewMixin, ComposeJsonResponseMixin, DetailView):
    tpl_path = 'composedetailview'

    # Do pomyslenia, czy w settingsach nie dac
    # ustawienia domyslnego prefixu dla detaila
    #url_prefix = 'detail'

    def pre_json_response(self, **kwargs):
        self.object = self.get_object()

    def get_object(self):
        obj = super().get_object()
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj


class ComposeListView(ComposeView, ComposeTemplateViewMixin, ComposeJsonResponseMixin, ListView):
    pass


class ComposeFormView(ComposeView, ComposeTemplateViewMixin, FormView):
    pass


class ComposeModelFormMixin(ComposeTemplateViewMixin):
    cancel_url = None
    fields = None
    exclude = None
    widgets = None

    def get_cancel_url(self):
        """Return the cancel URL to redirect to after cancelling a form."""
        if not self.cancel_url:
            raise ImproperlyConfigured("No URL to redirect to after cancel. Provide a cancel_url.")
        return str(self.cancel_url)  # cancel_url may be lazy  

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cancel_url'] = self.get_cancel_url()
        return context
    
    def get_fields(self):
        return self.fields

    def get_exclude(self):
        return self.exclude

    def get_widgets(self):
        return self.widgets

    def get_form_class(self):
        """Return the form class to use in this view - copied from django.vievs.generic.edit.ModelFormMixin and customized"""
        fields = self.get_fields()
        exclude = self.get_exclude()
        widgets = self.get_widgets()
        if fields is not None and self.form_class:
            raise ImproperlyConfigured(
                "Specifying both 'fields' and 'form_class' is not permitted."
            )
        if self.form_class:
            return self.form_class
        else:
            if self.model is not None:
                # If a model has been explicitly provided, use it
                model = self.model
            elif getattr(self, 'object', None) is not None:
                # If this view is operating on a single object, use
                # the class of that object
                model = self.object.__class__
            else:
                # Try to get a queryset and extract the model class
                # from that
                model = self.get_queryset().model

            if fields is None:
                raise ImproperlyConfigured(
                    "Using ComposeModelFormMixin (base class of %s) without "
                    "the 'fields' attribute is prohibited." % self.__class__.__name__
                )

            return model_forms.modelform_factory(model, fields=fields,
                                                 exclude=exclude, widgets=widgets)

class ComposeCreateView(ComposeView, ComposeModelFormMixin, CreateView):
    tpl_path = 'composecreateview'
    url_prefix = 'create'
    

class ComposeUpdateView(ComposeView, ComposeModelFormMixin, UpdateView):
    tpl_path = 'composeupdateview'
    url_prefix = 'update'

    def get_cancel_url(self):
        """Defaults to success url"""
        return self.get_success_url()

    def get_object(self):
        obj = super().get_object()
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj
    
class ComposeDeleteView(ComposeView, ComposeModelFormMixin, DeleteView):
    tpl_path = 'composedeleteview'
    url_prefix = 'delete'
