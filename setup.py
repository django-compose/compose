"""
https://codelab.pl
https://gitlab.com/django-compose/compose
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

EXCLUDE_FROM_PACKAGES = ['docs', 'tests', 'compose.bin', 'compose.conf.project_template']
    
setup(
    name='compose',
    version='0.0.1',
    description='Django Compose - lets handle the data',
    long_description=long_description,
    url='https://gitlab.com/django-compose/compose.git',
    author='codelab.pl',
    author_email='codelab@codelab.pl',
    scripts=['compose/bin/compose-admin.py'],
    entry_points={'console_scripts': [
        'compose-admin = compose.core.management:execute_from_command_line',
    ]},
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Webapps',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    keywords='django data-processing complex',
    packages=find_packages(exclude=EXCLUDE_FROM_PACKAGES),
    install_requires=['django'],
    project_urls={
        'Source': 'https://gitlab.com/django-compose/compose',
        'Bug Reports': 'https://gitlab.com/django-compose/compose',
        'Say Hello!': 'https://codelab.pl',
    },
)
